import './ListeContact.css';
import Listes from './ListeContact_DataSource';

const Liste_c = Listes.map((item) =>
    
    <tr key={item.id}>
        <td>{item.nom} - {item.numero}</td>
        <td> <a href="">Lire</a></td>
        <td> <a href="">Modifier</a></td>
        <td> <a href="">Supprimer</a></td>
    </tr>
);

 const ListeContact = ()=>{
    return (
        <div className="container_liste_c">
            <div className="header">
                <div className="header_content">
                    <div>
                        <center>
                            Liste des contacts
                        </center>
                    </div>
                    <button className="retour">Retour</button>
                </div>
            </div>
            <div className="content_liste_c">
                <div className="content_search">
                    <input type="text" name="search" placeholder="Rechercher"/>
                </div>
                <div className="content_all">
                    
                    <table>
                        {Liste_c}
                    </table>
                </div>
            </div>
            <div className="footer">
                <button>Trier</button>
                <button>Exporter</button>
            </div>
        </div>
    )
  }

  export default ListeContact;