import './ListeContact.css';

 const AjouterContact = ()=>{
    return (
        <div className="container_liste_c">
            <div className="header">
                <div className="header_content">
                    <div>
                        <center>
                            Nouveau Contact
                        </center>
                    </div>
                    <button className="retour">Retour</button>
                </div>
            </div>
            <div className="content">
                <form>
                    <input type="text" placeholder="Entrez Le nom"/>
                    <input type="text" placeholder="Numéro de téléphone"/>
                    <input type="email" placeholder="Entrez votre Email"/>
                    <button>Enregistrer</button>
                </form>
            </div>

        </div>
    )
  }

  export default AjouterContact;